# Configuring FOCT Forms
The finished product resides in index.html.
The working form is form.html
Field samples are s1.html to s5.html
Start by editing form.html by copy and pasting html from sample files as desired.
Configure associated field names inside FOCDMetadataForm.js file for the editor fields
Test Form.html > right click on it > Open with Live Server
When happy with results, copy and paste html into index.html
Test index.html, when happy upload to cloud.
Text cloud deployment at https://app.rlan.ca/focd

# Excel Client File
Use this file to construct your picklists for fields that require a dropdown pick list.
See inside the file for instructions how to map Excel Tags Sheet to Form Fields
Once completed in Excel, you need to update GCIP cache. Simply drag and drop the 
the excel file on the Investment Planner web page.
Get the file here
https://drive.google.com/drive/u/0/folders/1WjTV5nxdciduMaHRExiv2dizLOCZC_PD

# Field Types

## Input Field with PickList

<div class="input-field col s4">
    <input id="tbMDPortfolio_mdAdv" type="text" class="validate mcttUpdatablev1" 
    data-formkey="md-mdAdv-Portfolios">
    <label for="tbMDPortfolio_mdAdv">Investment Portfolio</label>
</div>


## Input Field with no pick list

<div class="input-field col s4">
    <input title="Enter free form text" id="tbMDContactNumber_mdAdv" type="text" class="validate updatableStd"
        data-formkey="md-mdAdv-na">
    <label for="tbMDContactNumber_mdAdv">Contact Info</label>
</div>

## Input Field with People Picklist   

<div class="input-field col s4">
    <input id="tbMDPrimaryContact_mdAdv" type="text" class="validate mcttUpdatablev1"
        data-formkey="people-mdAdv-Primary Contact">
    <label for="tbMDPrimaryContact_mdAdv">Primary Contact</label>
</div>


## Field with text area (free form no editor)

<div class="input-field col s12">
    <textarea data-formkey="md-mdAdv-na"
        id="xxx"
        class="materialize-textarea"> </textarea>
    <label for="xxx">Please enterxxx
        </label>
</div>

## Fields with text Editor

<div class="col s6">
    <h4
        style="font-size: 18px; margin-bottom: 3px;margin-top: 0px; cursor: pointer;">
        Field heading label</h4>
    <div id="dbFieldName_formName" class="editor"
        data-formkey="md-FormName-na">
    </div>
</div>

# References
HTML Reference - https://developer.mozilla.org/en-US/docs/Web/HTML/Element
CSS Grid - https://materializecss.com/grid.html
Text Inputs - https://materializecss.com/text-inputs.html


# How to configure fields
Gain a general highlevel understanding of HTML and CSS per above 3 reference links

## The following are FOCD rules for configuring HTML and CSS (no HTML or CSS knowlege needed)

"col sx": Materialize CSS Grid class that provides width (x= 4, 6,8 or 12), change to suit
"data-formkey": Change to suit rules below
id=": The id entered here maps to the database, change to suit rules below
class="editor": Add this to a div element to provide editor.
class="input-field" - add this to a div element to provide free form text area.
class="input-field: use in conjunction with input element to provide look and feel
input id=": standard HTML input element, id must match databse field name.
type="text", type="number": standard html input element enforces text only or number only.
class="validate mcttUpdatablev1": used on fields that require picklist.
label for="x">Field Name</label> Where x matches associated ID, Field name is free form, can be anything
class="validate updatableStd": use on input fields with no picklist


