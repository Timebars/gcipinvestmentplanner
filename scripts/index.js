



/* Initialize the databse */
openDB();


/* menu bar excel icon button */
$(document).on('click', '.btnExportAlltoCSV', function () {
  exportAllDataAsCSV()

});

/* Menu Bar Launch DM Lists */
$(document).on('click', '.btnViewDMLists', function (e) {

  document.getElementById("ipSupportPage").style.display = "none";
  document.getElementById("mdFormAdvL1").style.display = "none";
  $('.tabs').tabs();
});

/* home and default page when app opens */
$(document).on('click', '.launchTbHomePage', function (e) {
  document.getElementById("ipSupportPage").style.display = "none";
  document.getElementById("mdFormAdvL1").style.display = "block"; /* mod */
});


$(document).on('click', '.launchIpSupportPage', function (e) {
  document.getElementById("ipSupportPage").style.display = "block";
  document.getElementById("mdFormAdvL1").style.display = "none";
});


// Admin Panel Demo data menu menu bar init
document
  .getElementById("btnPopulateDemoData")
  .addEventListener("click", populateDemoData);

document
  .getElementById("btnclearObjectStores")
  .addEventListener("click", clearProjectDataTbBlMD);

document
  .getElementById("btnRefreshScreen")
  .addEventListener("click", reloadPage1);

function reloadPage1() {
  location.reload();
}

/* Admin panel export multi json files */
$(document).on('click', '#btnExportAll', function () {
  exportAllDataAsJsonFiles()

});